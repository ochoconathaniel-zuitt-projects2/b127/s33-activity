const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({

	name: {

		type:String,
		required : [true,"Course is required"]
	}

})

module.exports = mongoose.model("Course", courseSchema)